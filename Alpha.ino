
#include "FastLED.h"

// fast led constants
#define BRIGHTNESS  50
#define COLOR_ORDER GRB
#define DATA_PIN    3
#define LED_TYPE    WS2812B
#define NUM_LEDS    125
#define UPDATES_PER_SECOND 100
CRGB leds[NUM_LEDS];

#define BUTTON_PIN_2  2
#define BUTTON_PIN_5  5

int currentMode = BUTTON_PIN_2;
int previousMode = BUTTON_PIN_2;
int currentNumber = 0;

int currentState = 0;


unsigned long previousButton2Millis = 0;
unsigned long previousButton5Millis = 0;
unsigned long previousModeChangeMillis = 0;
unsigned long modeChangeInterval = 500;
unsigned long singleButtonInterval = 300;
int currentNumber2 = 0;
int currentNumber5 = 0;
void readButtons()
{
  int button_2_pressed = !digitalRead(BUTTON_PIN_2);
  if (button_2_pressed) {
    if (previousMode == BUTTON_PIN_2 && (millis() - previousButton2Millis >= singleButtonInterval)) {
      previousButton2Millis = millis();
      next_2_State();
      currentNumber = currentNumber2;
    }
    if (millis() - previousModeChangeMillis >= modeChangeInterval) {
      previousModeChangeMillis = millis();
      previousMode = currentMode;
      currentMode = BUTTON_PIN_2;
    }
  }

  
  int button_5_pressed = !digitalRead(BUTTON_PIN_5);
  if (button_5_pressed) {
    if (previousMode == BUTTON_PIN_5 && (millis() - previousButton5Millis >= singleButtonInterval)) {
//      Serial.print("#5-");
//      Serial.println(currentNumber5);
      previousButton5Millis = millis();
      next_5_State();
      currentNumber = currentNumber5;
    }
    if (millis() - previousModeChangeMillis >= modeChangeInterval) {
      previousModeChangeMillis = millis();
      previousMode = currentMode;
      currentMode = BUTTON_PIN_5;
//      Serial.println("m5");
    }
  }
}

int num_2_States = 2;
void next_2_State()
{
  currentNumber2++;
  currentNumber2 = currentNumber2 % num_2_States;
}

int num_5_States = 5;
void next_5_State()
{
  currentNumber5++;
  currentNumber5 = currentNumber5 % num_5_States;
}


CRGBPalette16 currentPalette;
TBlendType    currentBlending;


void setup()
{
  delay(2000); // power safe(r) boot
  Serial.begin(9600);
  pinMode(BUTTON_PIN_2, INPUT);
  digitalWrite(BUTTON_PIN_2, HIGH);
  pinMode(BUTTON_PIN_5, INPUT);
  digitalWrite(BUTTON_PIN_5, HIGH);
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  currentPalette = RainbowColors_p; //RainbowStripeColors_p;
  currentBlending = LINEARBLEND;
//  dark();
}

int getAnimationNumber() {
  int mode = currentMode * 10;
  int animationNumber = mode + currentNumber;
  return animationNumber;
}


unsigned long prevTime = 0;
void loop()
{
  readButtons();
  currentState = getAnimationNumber();
  switch (currentState) {
    case 20:
      aSimple();
////      purpleStripe();
//      lightning();
      break;
//    case 21:
//      dazzle();
//      break;
//    case 50:
//      twinkleRainbow();
//      break;
//    case 51:
//      rainbowFill();
//      break;
//    case 52:
//      cloudFill();
//      break;
//    case 53:
////      experiment2();
//      forestFill();
//      break;
//    case 54:
//      lavaFill();
//      break;
    default:
      aSimple();
//      lightning();
      break;
  }
  FastLED.delay(1000 / UPDATES_PER_SECOND);
  ++prevTime;
}


void FillLEDsFromPaletteColors(uint8_t colorIndex)
{
  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette(currentPalette, colorIndex, BRIGHTNESS, currentBlending);
    colorIndex += 3;
  }
}
// This function fills the palette with totally random colors.
void SetupTotallyRandomPalette()
{
  for ( int i = 0; i < 16; i++) {
    currentPalette[i] = CHSV( random8(), 255, random8());
  }
}
