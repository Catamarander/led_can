
#define BRANCH_CHANCE           2
#define BRANCH_MIN_HEIGHT       4

const int BRANCH_PATTERN[] = {7, 8, 15}; // Nearest "down" neighbors for this project
const int maxRows = 16;
const int boltInterval = 7;
unsigned long previousBoltMillis = 0;
void bolt(int sum, int row)
{
  if (sum < NUM_LEDS and row < maxRows) {
    if ((millis() - previousBoltMillis) >= boltInterval) {
      leds[sum] = CRGB::White;
      FastLED.show();
      int randNext = random(3);
      int next = BRANCH_PATTERN[randNext];
      sum = sum + next;
      row++;
      previousBoltMillis = millis();
      if (row > BRANCH_MIN_HEIGHT) {
        int randBranch = random(0, 10);
        if (randBranch < BRANCH_CHANCE) {
          bolt(sum + 15, row);
        }
      }
    }
    bolt(sum, row);
  }
}
