

long twinkleRainbowCounter = 0;
byte twinkleRainbowDivisor = 3;
long twinkleRainbowLimit = twinkleRainbowDivisor * 255;
byte twinkleRainbowFillSpeed() {
  twinkleRainbowCounter++;
  if (twinkleRainbowCounter > twinkleRainbowLimit) {
    twinkleRainbowCounter = 0;
  }
  return twinkleRainbowCounter / twinkleRainbowDivisor;
}

void twinkleRainbow() {
  currentPalette = RainbowStripeColors_p;
  currentBlending = LINEARBLEND;
  static uint8_t startIndex = 0;
  startIndex = twinkleRainbowFillSpeed(); /* motion speed */
  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
}


int rainbowFillSpeed = 2;
void setRainbowSpeed() {
  if (rainbowFillSpeed == 2) {
    rainbowFillSpeed = 3;
  }
  else if (rainbowFillSpeed == 3) {
    rainbowFillSpeed = 2;
  } 
}

void colorFill(CRGBPalette16 palette) {
  currentPalette = palette;
  currentBlending = NOBLEND;
  static uint8_t startIndex = 0;
  setRainbowSpeed();
  startIndex = startIndex + rainbowFillSpeed; /* motion speed */
  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
}
void rainbowFill() {
  colorFill(RainbowColors_p);
}

void cloudFill() {
  colorFill(CloudColors_p);
}

void forestFill() {
  colorFill(ForestColors_p);
}

void lavaFill() {
  colorFill(LavaColors_p);
}



void experiment2() {
  static uint8_t starthue = 0;
  fill_rainbow(leds, NUM_LEDS, --starthue, 20);
}



