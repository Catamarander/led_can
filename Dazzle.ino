
#define DAZZLE_DENSITY 4      // How many white lights are on synchronously

void dazzle()
{
  for (int j = 0; j < DAZZLE_DENSITY; j++) {
    leds[random(NUM_LEDS)] = CRGB::White;
  }
  FastLED.show();
  dark();
}
