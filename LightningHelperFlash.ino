// lightning flash
#define LONG_FLASH_SPEED  80
#define SHORT_FLASH_SPEED 15
const int FLASH_COUNTS[] = {0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 8, 9};
const int LEN_FLASH_COUNTS = sizeof(FLASH_COUNTS) / sizeof(int);

int numFlashes = getNumFlashes();
int flashSpeed = random(SHORT_FLASH_SPEED, LONG_FLASH_SPEED);
unsigned long previousFlashMillis = 0;
int flash_count = 0;

int getNumFlashes() {
  int i = random(LEN_FLASH_COUNTS);
  return FLASH_COUNTS[i];
}

void flashes()
{
  if (flash_count < numFlashes) {
    if (millis() - previousFlashMillis >= flashSpeed) {
      flashSpeed = random(SHORT_FLASH_SPEED, LONG_FLASH_SPEED);
      flash_count++;
      flash();
      previousFlashMillis = millis();
    }
    flashes();
  }
  else {
    flash_count = 0;
    numFlashes = getNumFlashes();
  }
}

void flash()
{
  flash_color(CRGB::White);
}

void flash_color(CRGB color)
{
  fill_solid(leds, NUM_LEDS, color);
  FastLED.show();
  dark();
}

void dark()
{
  fill_solid(leds, NUM_LEDS, CRGB::Black);
  FastLED.show();
}
