

const int BRANCH_PATTERN2[] = {7, 8, 15};

int simple_pos = random(6);
int next_start = 0;
int next_bolt = 0;
void aSimple() {
  if (prevTime < next_bolt || prevTime % 5 != 0) return;
  leds[simple_pos] = CRGB::White;
  FastLED.show();
  simple_pos += BRANCH_PATTERN2[random(3)];
//  Serial.println(simple_pos);
  if (simple_pos >= NUM_LEDS) {
    dark();
    simple_pos = random(7);
    next_bolt = prevTime + 5*(4+random(121));
  }
}

