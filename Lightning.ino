
// hack for distributing strike times with occassional very short and very long periods
const int SHORT_STRIKE_TIMES[] = {700, 1000, 2000, 3000, 3000, 3000, 3000, 4000};
const int LONG_STRIKE_TIMES[] = {5000, 5000, 5000, 6000, 7000, 7000, 10000};
const int LEN_SHORT_STRIKE_TIMES = sizeof(SHORT_STRIKE_TIMES) / sizeof(int);
const int LEN_LONG_STRIKE_TIMES = sizeof(LONG_STRIKE_TIMES) / sizeof(int);

#define SECONDARY_BOLT_CHANCE   2
#define SECONDARY_FLASH_CHANCE  3

int getStrikeDelay() {
  int i = random(LEN_SHORT_STRIKE_TIMES);
  int j = random(LEN_LONG_STRIKE_TIMES);
  return random(SHORT_STRIKE_TIMES[i], LONG_STRIKE_TIMES[j]);
}

int lightningInterval = getStrikeDelay();
unsigned long previousLightningMillis = 0;
void lightning()
{
  if ((millis() - previousLightningMillis) >= lightningInterval) {
    flashes();
    int startingPos = random(8);
    bolt(startingPos, 0);
    dark();
    int randFlash = random(10);
    if (randFlash < SECONDARY_FLASH_CHANCE) {
      flash();
    }
    int randSecondaryBolt = random(10);
    if (randSecondaryBolt < SECONDARY_BOLT_CHANCE) {
      bolt(startingPos, 0);
    }
    dark();
    previousLightningMillis = millis();
    lightningInterval = getStrikeDelay();
  }
  else {
    dark();
  }
}
